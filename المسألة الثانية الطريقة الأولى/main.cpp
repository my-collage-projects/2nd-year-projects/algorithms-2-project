#include <iostream>
#include <bits/stdc++.h>
using namespace std;

pair <int,int> Domino[100000];
vector<int >graph[100000];
int n;
int s,d;
int start_id=0;

void Build_graph()
{
cin>>n;
for(int i=0;i<n;i++)
{
cin>>Domino[i].first;
cin>>Domino[i].second;

}
for(int j=0;j<n;j++)
for(int i=0;i<n;i++)
    if(i!=j)
    if(Domino[j].first==Domino[i].first ||Domino[j].first==Domino[i].second ||Domino[i].first==Domino[j].second ||Domino[i].second==Domino[j].second)
graph[j].push_back(i);

}



long long start_dis[150];
bool start_visit[150]={false};

void start_BFS(int x)
{
queue <int> q;
q.push(x);
start_dis[x]=0;
while(!q.empty())
{
int f=q.front();
q.pop();
start_visit[x]=true;
int k;
for(int i=0;i<graph[f].size();i++)
{
k=graph[f][i];
if(start_dis[f]+1 < start_dis[k])
start_dis[k]=start_dis[f]+1;
if(start_visit[k]==false)
q.push(k);
start_visit[k]=true;
}
}

}

long long end_dis[150];
bool end_visit[150]={false};

int End_id=100000000000;
void End_BFS(int x)
{
    if(x>=n)
        return;
queue <int> q;
q.push(x);
end_dis[x]=0;
while(!q.empty())
{
int f=q.front();
q.pop();
end_visit[x]=true;
int k;
for(int i=0;i<graph[f].size();i++)
{
k=graph[f][i];
if(end_dis[f]+1 < end_dis[k])
end_dis[k]=end_dis[f]+1;
if(end_visit[k]==false)
q.push(k);
end_visit[k]=true;
}
}

}

int length=-1;

int shortest_path(int d)
{
long long MIN=20000000000000;
    for(int i=0 ;i<n ;i++)
    {
        if(Domino[i].first==d || Domino[i].second==d)
            {
        if(MIN>start_dis[i])
        {
        End_id=i;
        MIN=start_dis[i];
            }
            }
   }
if(MIN<200000000000)
return MIN+1;

else
    return -1;

}

void check_new(int m,pair<int,int>New[])
{
int New_length=-1;

for(int i=0;i<m;i++)
{
bool check_visted[150]={false};
bool check_End_visted[150]={false};

queue <int>q;

int startdist=11110;
int enddis=11110;
int test=start_id;

q.push(test);
if(New[i].first==s||New[i].second==s)
{startdist=0;
   goto next;}
if(New[i].first==Domino[test].first||New[i].second==Domino[test].first||New[i].first==Domino[test].second||New[i].second==Domino[test].second)
    {
startdist=start_dis[test]+1;
   goto next;
    }

else{

check_visted[test]=true;

while(!q.empty())
    {
test=q.front();
q.pop();

for(int j=0;j<graph[test].size();j++)
    {
if(check_visted[graph[test][j]]==false)
{
check_visted[graph[test][j]]=true;

if(New[i].first==Domino[graph[test][j]].first||New[i].second==Domino[graph[test][j]].first||New[i].first==Domino[graph[test][j]].second||New[i].second==Domino[graph[test][j]].second)
{
startdist=start_dis[graph[test][j]]+1;
 goto next;
}
else
    q.push(graph[test][j]);
}
}
}
}

next:

int end_test=End_id;
queue <int>q_end;
q_end.push(end_test);
if(New[i].first==d||New[i].second==d)
{enddis=0;
   goto END;
}
if(New[i].first==Domino[end_test].first|| New[i].second==Domino[end_test].first||New[i].first==Domino[end_test].second||New[i].second==Domino[end_test].second)
    {
enddis=end_dis[end_test]+1;
   goto END;
    }

else{

check_End_visted[end_test]=true;

while(!q_end.empty())
    {
end_test=q_end.front();
q_end.pop();

for(int j=0;j<graph[end_test].size();j++)
    {
if(check_visted[graph[end_test][j]]==false)
{
check_visted[graph[end_test][j]]=true;

if(New[i].first==Domino[graph[end_test][j]].first||New[i].second==Domino[graph[end_test][j]].first||New[i].first==Domino[graph[end_test][j]].second||New[i].second==Domino[graph[end_test][j]].second)
{
enddis=end_dis[graph[end_test][j]]+1;
 goto END;
}
else
    q_end.push(graph[end_test][j]);
}
}
}
}

END: if(enddis<11110 && startdist<11110)
   {

    if(enddis+startdist<New_length || New_length<=0)
        New_length=startdist+enddis+1;
 while(!q.empty())
  q.pop();
while(!q_end.empty())
  q_end.pop();
}
}
if(New_length!=-1)
if(New_length<length || length<0 )
    length=New_length;

}
int main()
{

Build_graph();
for(int i=0;i<n;i++)
{
    start_dis[i]=200000000000;
    end_dis[i]=200000000000;
}

int m;
pair<int,int> New[150];

cin>>m;
   for(int i=0 ;i<m ;i++)
{
cin>>New[i].first;
cin>>New[i].second;
     }
cin>>s>>d;

for(int i=0;i<n;i++)
if(Domino[i].first==s ||Domino[i].second==s)
    start_id=i;

start_BFS(start_id);

length=shortest_path(d);

End_BFS(End_id);
check_new(m,New);
cout<<endl<<length;
}
