#include <iostream>
#include <math.h>
#include <queue>
#include <vector>
using namespace std;


struct Node
{
	int s,BF,l,R,level , Lv,Rv;
	Node* left;
	Node* right;
};

int n;
Node* root=NULL;

void build_BST(Node* &root);
int add(int x, Node* &root , int L);
void PrintBF(Node* T);
void PrintBFS(Node* T);
void rotate_left(Node* &t);
void rotate_right(Node* &t);
void fix(Node* &t);
int check_salaries(int x,Node* root);
int search_to_fix(int x,Node* &rt);

int add(int x, Node* &root , int L)
{
	if(root == NULL)
	{
		root = new Node;
		root->s = x;
		root->left = root->right = NULL;
		root->BF = 0;
		root->Lv = root->Rv = 0;
		root->level = L;
		root->l = root->R = 0;
		cout << "Added" << endl;
		return 1;
	}
//	if(root->s == x)
//	{
//		Node* temp= new Node;
//		temp->s= x;
//		temp->right = NULL;
//		temp->left = root->left;
//		root->left = temp;
//	}
	else if(root->s > x)
	{
		int nLv;
		root->l++ ;
		nLv = add(x, root->left, L+1);
		root->Lv = max(root->Lv,nLv);
		root->BF = root->Lv-root->Rv;
		return max(root->Rv , root->Lv) +1; 
	}
	else if(root->s <= x)
	{
		int nRv;
		root->R++;
		nRv= add(x, root->right, L+1);
		root->Rv = max(root->Rv,nRv);
		root->BF = root->Lv - root->Rv;
		return max(root->Rv , root->Lv) +1;
	}
}

void PrintBF(Node* T)
{
    queue<Node*> q;
    q.push(T);
    while(!q.empty())
    {
        T = q.front();
        cout << T->BF;
        q.pop();
        if(T->left != NULL)
            q.push(T->left);
        if(T->right != NULL )
            q.push(T->right);
    }
}

void PrintBFS(Node* T)
{
    queue<Node*> q;
    q.push(T);
    while(!q.empty())
    {
        T = q.front();
        cout << T->s;
        q.pop();
        if(T->left != NULL)
            q.push(T->left);
		else
		{
			Node* tmp=new Node;
			tmp->s=0;
			tmp->left = tmp->right = 0;

		}
        if(T->right != NULL )
            q.push(T->right);
		else
		{			Node* tmp=new Node;
			tmp->s=0;
			tmp->left = tmp->right = 0;
		}
    }
}

void rotate_left(Node* &t)
{
	cout << "rotate left" << endl;
	
	Node* temp=t;
	
	cout << temp->s  << "**" ; 
	
	t=t->right;
	
	cout <<t->s << endl;
	
	temp->right = t->left;
	temp->Rv = t->Lv;
	temp->R = t->l;
	t->left=temp;
	t->l += (temp->l +1);
	t->Lv = max(temp->Lv , temp->Rv) + 1;																						
	temp->level--;
	t->level++;
	temp->BF = (temp->Lv) - (temp->Rv); 
	t->BF = t->Lv - t->Rv;
	cout << t->s << "BF=" << t->BF;
	cout << temp->s << "BF=" << temp->BF;
}

void rotate_right(Node* &t)
{
	cout << "rotate right" << endl;

	Node* temp=t;

	t=t->left;
	temp->left = t->right;
	temp->Lv = t->Rv;
	temp->l = t->R;
	t->right=temp;
	t->R += (temp->R +1);
	t->Rv = max(temp->Lv , temp->Rv) + 1;																						
	temp->level--;
	t->level++; 

	temp->BF = temp->Lv - temp->Rv; 
	t->BF = t->Lv - t->Rv;
	cout << t->s << "BF=" << t->BF<<endl;
	cout << temp->s << "BF=" << temp->BF << endl;

}

void fix(Node* &t)
{
	if(abs(t->BF)<=1)
		return;
	if(t->BF==-2)
		if(t->right->BF < 0)
		{
			rotate_left(t);
		}
		else 
		{
			rotate_right(t->right);
			rotate_left(t);
		}
	else if(t->BF==2)
		if(t->left->BF > 0)
		{
			rotate_right(t);
		}
		else 
		{	
			rotate_left(t->left);
			rotate_right(t);
		}
}

void build_BST(Node* &root)
{
	int x;
	for(int i=0;i<n;i++)
	{
		cin >> x;
		add(x, root,0);
		search_to_fix(x,root);
		cout << endl;
		PrintBF(root);
		cout << "``````````````````````````" << endl;
		PrintBFS(root);
	}
}

int check_salaries(int x,Node* root)
{
	if(root==NULL)
		return 0;
	if(root->s == x)
		return root->R;
	else if(root->s > x)
	{
		cout << endl << root->R << "**"<< endl;
		return check_salaries( x,root->left) + 1 + root->R;
	}
	else
	{
		return check_salaries( x,root->right) ;
	}
}

int search_to_fix(int x,Node* &rt)
{
	cout << "searching" << endl;
	if (rt==NULL)
		return 0 ;
	if(abs(rt->BF) > 1)
	{
		fix(rt);
		return 1;
	}
	if(rt->s >= x) 
	{
	    int L= search_to_fix(x,rt->left);
		rt->BF -= L;
		return L ;
	}
	else 
	{
		int R= search_to_fix(x,rt->right);
		rt->BF += R;
		return R;
	}
	
}

int main()
{
	int m,c,num;
	vector<int> v;
	cin >> n;
	build_BST(root);
	PrintBFS(root);
	cin >> m;
	for(int i=0 ; i<m ;i++)
	{
		cin >> c;
		if(c==1)
		{
			cin >> num;
			n++;
			add(num,root,0);
			search_to_fix(num,root);
		}
		else 
		{
			cin >> num;
			v.push_back(check_salaries(num,root));
		}
	}
	cout << "answer   ````````````````````````````````````````````````" << endl <<endl;
	for(unsigned int i=0;i<v.size();i++)
	{
		cout << v[i] << endl;
	}
	//PrintBFS(root);
	system("pause");
}