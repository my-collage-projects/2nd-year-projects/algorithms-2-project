#include <iostream>
#include <bits/stdc++.h>

using namespace std;

struct tnode
{
   int data ;
   int number_right;
   tnode * left ;
   tnode * right ;
};

tnode * newnode ( int data )
{
    tnode * node = new tnode () ;
    node -> data = data ;
    node -> left = node -> right = NULL ;
    return node ;
}


void marge (int *arr,int st,int mid,int ed)
{
    int i,j,k;
    int n1=mid-st+1;
    int n2=ed-mid;
    int L[n1],R[n2];

    for(i=0;i<n1;i++)
        L[i]=arr[st+i];

    for(j=0;j<n2;j++)
        R[j]=arr[mid+1+j];

    i=0;
    j=0;
    k=st;

    while(i<n1&&j<n2)
    {
        if(L[i]<=R[j])
        {
            arr[k]=L[i];
            i++;
        }
        else
        {
            arr[k]=R[j];
            j++;
        }
        k++;
    }

    while(i<n1)
    {
        arr[k]=L[i];
        i++;
        k++;
    }

    while(j<n2)
    {
        arr[k]=R[j];
        j++;
        k++;
    }

}



void margesort(int arr[],int l,int r)
{
    if(l<r)
    {
        int m=l+(r-l)/2;
        margesort(arr,l,m);
        margesort(arr,m+1,r);
        marge(arr,l,m,r);
    }
}


tnode * bbst (int arr[] , int st , int ed )
{
    if (st > ed )
        return NULL ;
    int m = (st+ed)/2 ;
    tnode * root =  newnode(arr[m]);
        root->number_right = ed-m ;


    root -> right = bbst (arr , m+1 , ed);

    root -> left = bbst (arr , st , m-1);

    return root ;
}


tnode * INSERT (tnode *root , int data )
{
    if(root == NULL )
        root =newnode (data);

    else if (data <= root -> data)
    {   root -> number_right = root -> number_right;
        root->left =INSERT(root->left , data);
    }
    else if (data >root-> data)
    {   root -> number_right= root -> number_right + 1 ;
        root->right = INSERT (root->right , data );
    }

    return root;
};


 int SEARCH ( tnode * root , int data)
{
    if (root == NULL)
        return 0 ;

    if ( root -> data ==data)
        return root ->number_right ;

    else if ( data < root -> data )
       return SEARCH (root->left , data) + root->number_right +1 ;

   else if (data > root->data)
        return SEARCH (root->right ,data)  ;

}


int main()
{
    int n;
    cin>>n;
    int arr [n];
    for(int i=0;i<n;i++)
      cin>>arr[i];


    margesort(arr,0,n);

    tnode* root = bbst (arr,0,n-1);

    int a ;
    cin>>a;
    for(int k=1;k<=a;k++)
     {

         int number ;
         cin>>number ;
         if (number == 1)
           {
             int salary;
             cin>>salary;

             root= INSERT ( root, salary  );

            }

     if (number == 2)
      {
          int salary;
          cin>>salary;
          cout<<"\n";
          cout<< SEARCH (root , salary) ;
      }

     }

    return 0;
}
