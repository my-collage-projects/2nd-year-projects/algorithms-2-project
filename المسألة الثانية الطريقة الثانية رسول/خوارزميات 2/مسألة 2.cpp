#include <iostream>
#include <vector>
#include < queue>

using namespace std;

struct Node
{
	int num,s,e;
	bool isExtra;
	Node(){s=-1;e=-1;}
};

int n,sn,m;
vector<Node> adj[200000];
pair<int,int> arr[200000];
int st,ed;

void print_graph()
{
	cout << "******************************";
	for( int i=0;i<sn+1;i++)
	{
		for(unsigned int j=0;j<adj[i].size();j++)
			cout << "---"<< adj[i][j].num ;
		cout << endl;
	}
}

void find (int start , int end)
{
	Node st,et;
	int slevel=1;
	int elevel=1;
	queue<Node> sq,eq;
	if(!adj[start].empty())
	{
		adj[start][0].s = 0;
		sq.push(adj[start][0]);
	}

	if(!adj[end].empty())
	{
		adj[end][0].e = 0;
		eq.push(adj[end][0]);
	}
	
	while(!sq.empty() || !eq.empty() )
	{
		if(!sq.empty())
		{
			st = sq.front();
		
			slevel = st.s;
			sq.pop();
			for(unsigned int i=1 ; i<adj[st.num].size();i++)
			{
				if(adj[adj[st.num][i].num][0].s==-1)
				{
					adj[st.num][i].s =slevel +1;
				
					adj[adj[st.num][i].num][0].s = slevel+1;
					
					sq.push(adj[st.num][i]);
				}				
			}
		}

		if(!eq.empty())
		{
			et = eq.front();
			
			elevel = et.e;
			eq.pop();
			for(unsigned int i=1;i<adj[et.num].size();i++)
			{
				if(adj[adj[et.num][i].num][0].e == -1 )
				{
				
					adj[et.num][i].e = elevel+1 ;		
					adj[adj[et.num][i].num][0].e = elevel +1;
					eq.push(adj[et.num][i]);
				}
			}
		}
	}
}

int test()
{
	int sol=10e5;
	int E=0,F=0;
	for(int i=0;i<2*m;i++)
	{
		if(arr[i].first == st)
		{
		F=0;
		if(arr[i].second == ed)
		{
			E = 0;
			sol = min (sol,E+F +1);
			
		}
		else if(!adj[arr[i].second].empty())
		{
			if(adj[arr[i].second][0].e != -1)
			{
				E = adj[arr[i].second][0].e ;
				sol = min (sol,E + F +1);
			}
		}
		else 
			continue;
		}
		else if(!adj[arr[i].first].empty())
		{
			if(adj[arr[i].first][0].s!=-1)
			   F = adj[arr[i].first][0].s;
			if(arr[i].second == ed )
				{
					E=0;
					sol = min (sol,E+F +1);
				}
			else if(!adj[arr[i].second].empty() )
			{
				if(adj[arr[i].second][0].e!= -1)
				{
					E=adj[arr[i].second][0].e;
					sol = min (sol,E+F +1);
				}
			}
			else
				continue;
		}
		}
		if(sol == 10e5 )
			return -1;
		else 
		{
			return sol;
		}


}

int main()
{
	cin >> n;
	pair <int , int > p;
	Node t1,t2;

	for(int i=0;i<n;i++)
	{
		cin >> p.first;
		cin >> p.second;
		t1.num =p.first;
		t2.num = p.second;
		if(adj[p.first].size() == 0)
			adj[p.first].push_back(t1);
		if(adj[p.second].size() == 0)
			adj[p.second].push_back(t2);
		adj[p.first].push_back(t2);
		adj[p.second].push_back(t1);
	
	}
	cin >> m;
	for(int i=0;i<2*m;i=i+2)
	{
		cin >> p.first;
		cin >> p.second;
		arr[i] = p;
		swap(p.first, p.second );
		arr[i+1] = p;
	}
	cin >> st>> ed;
	find(st,ed);
	 int sv=-1,t;
	 t= test();
	 if(!adj[st].empty() && !adj[ed].empty())
	 {
		 if(adj[st][0].e != -1)
		 sv = adj[st][0].e +1;
	 }
	 if(sv!=-1   && t!=-1)
		 cout << min(t,sv) ;
	 else if(t==-1)
		 cout << sv;
	  else if(sv == -1 )
		 cout << t;
	 
	 else
		 cout << "-1";
	system("pause");
	return 0;
}
